#!/usr/bin/python
import os
import time
import random
import robin_stocks as robin

def getLastOrderPrice():
	for data_point in robin.orders.get_all_crypto_orders():
		if data_point['state'] == 'filled':
					return data_point['average_price']
def getLastFilledOrderInfo():
	lastAction = ''
	for data_point in robin.orders.get_all_crypto_orders():
		if data_point['state'] == 'filled':
			if str(data_point['side']) == 'buy':
				lastAction = 'Bought' 
			else: lastAction = 'Sold'
			return 'Last completed order: ' + lastAction  +' '+ str(round(float(data_point['quantity']),8)) + 'BTC when the price was $' + str(round(float(data_point['average_price']),2))
def getOpenOrderInfo():
	for data_point in robin.orders.get_all_crypto_orders():
		if data_point['state'] == 'confirmed':
					return 'Active order: ' + str(data_point['side']).capitalize() +'ing '+ str(round(float(data_point['quantity']),8)) + 'BTC when the price hits $' + str(round(float(data_point['price']),2))

currentUSD = 0.00
earningsToday = 0.00
desiredGain = 0.02
desiredDip = -0.02
tradeAmount = 100.00
holdAmount = 0.2
def main():
	while True:
		#print(robin.orders.get_all_crypto_orders())
		#currentUSD = robin.account.load_phoenix_account(info='crypto_buying_power')['amount']
		#earningsToday = round(float(currentUSD)-float(startingUSD),2)
		#print('Current USD balance: $' + str(currentUSD)+', ($' + str(earningsToday)+' Gained/Lost)')
		currentHoldPrice = float(getLastOrderPrice())
		limitPrice = round(float(currentHoldPrice + (desiredGain*currentHoldPrice)),2)
		limitBuyPrice = round(float(currentHoldPrice + (desiredDip*currentHoldPrice)),2)
		currentMarkPrice = round(float(robin.get_crypto_quote("BTC","mark_price")),2)
		currentChange = round(((currentMarkPrice-currentHoldPrice)/currentHoldPrice)*100,2)
		currentHeld = round(float(robin.crypto.get_crypto_positions(info='quantity')[0]),8)
		currentOrders = len(robin.orders.get_all_open_crypto_orders())
		if currentHeld > 0:
			print('Currently holding: ' + str(currentHeld) + 'BTC')
		print('Current gain/dip: ' + str(currentChange) + "%")
		print('Current market price of BTC : $' + str(currentMarkPrice))
		print(getLastFilledOrderInfo())
		print('Active orders: ' + str(currentOrders))
		if currentOrders > 0:
			print(getOpenOrderInfo())
		if currentHeld > 0:
			if currentOrders == 0:
				print('Setting limit sell for: $'+str(limitPrice))
				print('Selling $' + str(currentHeld)+' BTC at $' + str(limitPrice))
				robin.orders.order_sell_crypto_limit("BTC", currentHeld, limitPrice, timeInForce='gtc')
			else:
				print ('Waiting for a ' + str(desiredGain*100) +'% gain before selling. Currently at ' + str(currentChange) + '%')			
		else:
			if currentOrders == 0:
				print('Setting limit buy for: $'+str(tradeAmount))
				print('Buying $' + str(tradeAmount/limitBuyPrice) + ' of BTC at $' + str(limitBuyPrice))
				robin.orders.order_buy_crypto_limit_by_price("BTC", tradeAmount, limitBuyPrice, timeInForce='gtc')
			else:
				print ('Waiting for a ' + str(desiredDip*100) +'% dip before buying. Currently at ' + str(currentChange) + '%')
				
		timerWait=(30+random.randint(0, 30))
		print(str(timerWait) + ' sec sleep')
		time.sleep(timerWait)
		os.system('clear')
if __name__ == '__main__':
	robin.login(os.environ.get('rhUser'),os.environ.get('rhPassword'))
	#startingUSD = robin.account.load_phoenix_account(info='crypto_buying_power')['amount']

	main()